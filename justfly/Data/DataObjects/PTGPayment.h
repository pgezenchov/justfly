//
//  PTGPayment.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGPayment : PTGJSONModel

@property (nonatomic, strong) NSNumber *paymentMethodId;
@property (nonatomic, strong) NSNumber *paymethodId;

@end

NS_ASSUME_NONNULL_END
