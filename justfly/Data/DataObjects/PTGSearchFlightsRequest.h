//
//  PTGSearchFlightsRequest.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGJSONModel.h"
#import "PTGFlight.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    PTGItinearyTypeCheapest,
    PTGItinearyTypeDirect,
    PTGItinearyTypeFlexible
} PTGItinearyType;

typedef enum : NSUInteger {
    PTGFlightTypeOneWay,
    PTGFlightTypeRoundTrip
} PTGFlightType;

@interface PTGSearchFlightsRequest : PTGJSONModel

@property (nonatomic, strong) NSString *departureAirport;
@property (nonatomic, strong) NSString *arrivalAirport;
@property (nonatomic, strong) NSString *departureDate;
@property (nonatomic, strong) NSString *arrivalDate;
@property (nonatomic, strong) NSString *flightType;
@property (nonatomic, strong) NSNumber *adults;
@property (nonatomic, strong) NSString *cabinClass;
@property (nonatomic, strong) NSNumber *pageIndex;
@property (nonatomic, strong) NSNumber *pageSize;
@property (nonatomic, strong) NSString *sortBy;
@property (nonatomic, strong) NSString *sortOrder;
@property (nonatomic, strong) NSString *currencyCode;
@property (nonatomic, strong) NSString *itinearyType;

- (instancetype)initWithDepartureAirportCode:(NSString *)fromAirportCode
                   arrivalAirportCode:(NSString *)arrivalAirportCode
                            departureDate:(NSString *)departureDate
                               returnDate:(NSString * _Nullable)returnDate
                               flightType:(PTGFlightType)flightType
                              adultsCount:(NSUInteger)adultsCount
                               cabinClass:(PTGCabinClass)cabinClass
                                pageIndex:(NSUInteger)pageIndex
                                 pageSize:(NSUInteger)pageSize
                             currencyCode:(NSString * _Nullable)currencyCode
                             itinearyType:(PTGItinearyType)itinearyType;

+ (NSString*)stringForItinearyType:(PTGItinearyType)itinearyType;

@end

NS_ASSUME_NONNULL_END
