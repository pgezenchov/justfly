//
//  PTGItineary.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGJSONModel.h"
#import "PTGFlight.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PTGFlight;

@interface PTGItineary : PTGJSONModel

@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSNumber *ticketsAvailable;
@property (nonatomic, strong) PTGFlight *outboundFlight;
@property (nonatomic, strong) PTGFlight *returnFlight;

@end

NS_ASSUME_NONNULL_END
