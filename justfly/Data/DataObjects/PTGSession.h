//
//  PTGSession.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGSession : PTGJSONModel

@property (nonatomic, strong) NSString *token;

@end

NS_ASSUME_NONNULL_END
