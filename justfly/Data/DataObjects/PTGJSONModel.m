//
//  PTGJSONModel.m
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGJSONModel.h"

@implementation PTGJSONModel

+ (NSArray*)objectFromDictionaries:(NSArray<NSDictionary*> *)dictionaries {
    NSMutableArray *objects = [NSMutableArray new];
    
    for (NSDictionary *dictionary in dictionaries) {
        id object = [[[self class] alloc] initWithDictionary:dictionary error:nil];
        
        if (object) {
            [objects addObject:object];
        }
    }
    
    return objects;
}

@end
