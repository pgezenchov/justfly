//
//  PTGBookingTransaction.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGItineary.h"
#import "PTGCustomerDetails.h"
#import "PTGPayment.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PTGExtra;

@interface PTGBookingTransaction : PTGItineary

@property (nonatomic, strong) PTGCustomerDetails *customerDetails;
@property (nonatomic, strong) PTGPayment *payment;

@end

NS_ASSUME_NONNULL_END
