//
//  PTGJSONModel.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "JSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGJSONModel : JSONModel

@property (nonatomic, strong) NSNumber<Optional> *id;

+ (NSArray*)objectFromDictionaries:(NSArray<NSDictionary*> *)dictionaries;

@end

NS_ASSUME_NONNULL_END
