//
//  PTGConnection.h
//  justfly
//
//  Created by Petar Gezenchov on 15/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGJSONModel.h"
#import "PTGAirline.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGConnection : PTGJSONModel

@property (nonatomic, strong) NSString *airport;
@property (nonatomic, strong) NSString *departureTime;
@property (nonatomic, strong) NSString *arrivalTime;
@property (nonatomic, strong) NSNumber<Optional> *airlineId;
@property (nonatomic, strong) PTGAirline<Optional> *airline;

@end

NS_ASSUME_NONNULL_END
