//
//  PTGUpgradeOption.h
//  justfly
//
//  Created by Petar Gezenchov on 15/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGUpgradeOption : PTGJSONModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *price;

@end

NS_ASSUME_NONNULL_END
