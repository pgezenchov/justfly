//
//  PTGSearchFlightsResponse.h
//  
//
//  Created by Petar Gezenchov on 13/04/2019.
//

#import "PTGJSONModel.h"
#import "PTGAirline.h"
#import "PTGBestPrice.h"
#import "PTGItineary.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PTGItineary;
@protocol PTGAirline;
@protocol PTGAirport;
@protocol PTGBestPrice;

@interface PTGSearchFlightsResponse : PTGJSONModel

@property (nonatomic, strong) NSArray<PTGItineary> *itinearies;
@property (nonatomic, strong) NSArray<PTGAirline> *airlines;
@property (nonatomic, strong) NSArray<PTGBestPrice> *bestPrices;

@end

NS_ASSUME_NONNULL_END
