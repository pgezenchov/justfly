//
//  PTGPaymethod.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGPaymethod : PTGJSONModel
@property (nonatomic, strong) NSString *label;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSDictionary *availableForPaymentMethods;
@property (nonatomic, strong) NSNumber *sort;
@end

NS_ASSUME_NONNULL_END
