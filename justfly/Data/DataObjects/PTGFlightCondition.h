//
//  PTGFlightCondition.h
//  justfly
//
//  Created by Petar Gezenchov on 15/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTGJSONModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGFlightCondition : PTGJSONModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *value;
@property (nonatomic, strong) NSNumber *enabled;

@end

NS_ASSUME_NONNULL_END
