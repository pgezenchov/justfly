//
//  PTGFlight.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGJSONModel.h"
#import "PTGAirport.h"
#import "PTGAirline.h"
#import "PTGConnection.h"
#import "PTGUpgradeOption.h"
#import "PTGFlightCondition.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    PTGCabinClassEconomyLight,
    PTGCabinClassEconomy,
    PTGCabinClassBusiness,
    PTGCabinClassFirst
} PTGCabinClass;

@protocol PTGConnection;
@protocol PTGUpgradeOption;
@protocol PTGFlightCondition;

@interface PTGFlight : PTGJSONModel

@property (nonatomic, strong) NSString<Optional> *flightNumber;
@property (nonatomic, strong) NSString *departureAirport;
@property (nonatomic, strong) NSString *arrivalAirport;
@property (nonatomic, strong) NSString *departureTime;
@property (nonatomic, strong) NSString *arrivalTime;
@property (nonatomic, strong) NSNumber *duration;
@property (nonatomic, strong) NSNumber *airlineId;
@property (nonatomic, strong) PTGAirline<Optional> *airline;
@property (nonatomic, strong) NSString<Optional> *price;
@property (nonatomic, strong) NSArray<PTGConnection> *connections;
@property (nonatomic, strong) NSArray<PTGUpgradeOption, Optional> *upgradeOptions;
@property (nonatomic, strong) NSArray<PTGFlightCondition, Optional> *flightConditions;


+ (NSString*)stringForCabinClass:(PTGCabinClass)cabinClass;

@end

NS_ASSUME_NONNULL_END
