//
//  PTGFlight.m
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGFlight.h"

@implementation PTGFlight

+ (NSString*)stringForCabinClass:(PTGCabinClass)cabinClass {
    switch (cabinClass) {
        case PTGCabinClassEconomyLight:
            return @"Economy Light";
            break;
        case PTGCabinClassEconomy:
            return @"Economy";
            break;
        case PTGCabinClassBusiness:
            return @"Business";
            break;
        case PTGCabinClassFirst:
            return @"First";
            break;
        default:
            return @"Economy Light";
            break;
    }
}

@end
