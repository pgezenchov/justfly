//
//  PTGFlightsSearchRequest.m
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGSearchFlightsRequest.h"


@implementation PTGSearchFlightsRequest

- (instancetype)initWithDepartureAirportCode:(NSString *)fromAirportCode
                   arrivalAirportCode:(NSString *)arrivalAirportCode
                            departureDate:(NSString *)departureDate
                               returnDate:(NSString * _Nullable)returnDate
                               flightType:(PTGFlightType)flightType
                              adultsCount:(NSUInteger)adultsCount
                               cabinClass:(PTGCabinClass)cabinClass
                                pageIndex:(NSUInteger)pageIndex
                                 pageSize:(NSUInteger)pageSize
                             currencyCode:(NSString * _Nullable)currencyCode
                             itinearyType:(PTGItinearyType)itinearyType {
    self = [super init];
    if (self) {
        _departureAirport = fromAirportCode;
        _arrivalAirport = arrivalAirportCode;
        _departureDate = departureDate;
        _arrivalDate = returnDate;
        _flightType = [PTGSearchFlightsRequest stringForFlightType:flightType];
        _adults = @(adultsCount);
        _cabinClass = [PTGFlight stringForCabinClass:cabinClass];
        _pageIndex = @(pageIndex);
        _pageSize = pageSize > 0 ? @(pageSize) : @(10);
        _currencyCode = currencyCode ? : @"USD";
        _itinearyType = [PTGSearchFlightsRequest stringForItinearyType:itinearyType];
    }
    return self;
}

+ (NSString*)stringForItinearyType:(PTGItinearyType)itinearyType {
    switch (itinearyType) {
        case PTGItinearyTypeCheapest:
            return @"cheapest";
            break;
        case PTGItinearyTypeDirect:
            return @"direct";
            break;
        case PTGItinearyTypeFlexible:
            return @"flexible";
            break;
        default:
            return @"cheapest";
            break;
    }
}

+ (NSString*)stringForFlightType:(PTGFlightType)flightType {
    switch (flightType) {
        case PTGFlightTypeOneWay:
            return @"one-way";
            break;
        case PTGFlightTypeRoundTrip:
            return @"round-trip";
            break;
        default:
            return @"one-way";
            break;
    }
}

@end
