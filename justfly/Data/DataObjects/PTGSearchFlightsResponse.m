//
//  PTGSearchFlightsResponse.m
//  
//
//  Created by Petar Gezenchov on 13/04/2019.
//

#import "PTGSearchFlightsResponse.h"

@implementation PTGSearchFlightsResponse

- (instancetype)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err {
    self = [super initWithDictionary:dict error:err];
    
    if (self) {
        NSMutableArray *allFlights = [[NSMutableArray alloc] init];
        
        for (PTGItineary *itineary in _itinearies) {
            [allFlights addObject:itineary.outboundFlight];
            [allFlights addObject:itineary.returnFlight];
        }
        
        for (PTGFlight *flight in allFlights) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"id == %@", flight.airlineId];
            flight.airline = [[_airlines filteredArrayUsingPredicate:predicate] firstObject];
            for (PTGConnection *connection in flight.connections) {
                predicate = [NSPredicate predicateWithFormat:@"id == %@", connection.airlineId];
                connection.airline = [[_airlines filteredArrayUsingPredicate:predicate] firstObject];
            }
            
        }
    }
    
    return self;
}

@end
