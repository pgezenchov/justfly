//
//  PTGServiceParameters.m
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGServiceParameters.h"

@implementation PTGServiceParameters

+ (NSString *)urlForEnvironment:(PTGServiceEnvironment)environment {
    switch (environment) {
        case PTGServiceEnvironmentDev:
            return @"";
            break;
        case PTGServiceEnvironmentStage:
            return @"";
            break;
        case PTGServiceEnvironmentLive:
            return @"";
            break;
    }
}

+ (NSString *)textForError:(PTGServiceError)error {
    NSString *errorText = @"Unknown error";
    
    switch (error) {
        case PTGServiceErrorOffline:
            errorText = NSLocalizedString(@"You are offline. Check your internet connection.", @"Error message - offline");
            break;
        case PTGServiceErrorTimeout:
            errorText = NSLocalizedString(@"Request timed out.", @"Error message - timeout");
            break;
        case PTGServiceErrorInvalidResponse:
            errorText = NSLocalizedString(@"Server returned invalid response", @"Error message - invalid response");
            break;
        default:
            break;
    }
    
    return errorText;
}

@end
