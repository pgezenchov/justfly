//
//  PTGServiceParameters.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

// Environments
typedef NS_ENUM(NSInteger, PTGServiceEnvironment) {
    PTGServiceEnvironmentDev,
    PTGServiceEnvironmentStage,
    PTGServiceEnvironmentLive
};

// Internal service error codes
typedef NS_ENUM(NSInteger, PTGServiceError) {
    // Internal
    PTGServiceErrorUnknown              = -100,
    PTGServiceErrorSerialization        = -110,
    PTGServiceErrorTimeout              = -120,
    PTGServiceErrorOffline              = -130,
    PTGServiceErrorInvalidResponse      = -140,
};

@interface PTGServiceParameters : NSObject

+ (NSString*)urlForEnvironment:(PTGServiceEnvironment)environment;

+ (NSString *)textForError:(PTGServiceError)error;

@end

NS_ASSUME_NONNULL_END
