//
//  PTGServiceClient.m
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGServiceClient.h"
#import "PTGDummyResponseFactory.h"

@interface PTGServiceClient ()

@property (nonatomic, strong) AFHTTPSessionManager  *sessionManager;
@property (nonatomic, strong) NSString *baseURL;

@end

static AFNetworkReachabilityStatus  kPTGService_NetworkStateStatus;

@implementation PTGServiceClient

NSString * const kPTGServiceApplicationToken = @"UIYJHcnTL6KtYCx506aHBdOHmzEroidk";

#pragma mark Constructors

- (instancetype)init
{
    return [self initWithEnvironment:PTGServiceEnvironmentDev
                             session:nil];
}

- (instancetype)initWithEnvironment:(PTGServiceEnvironment)environment
                            session:(PTGSession*)session {
    self = [super init];
    if (self) {
        _baseURL            = [PTGServiceParameters urlForEnvironment:environment];
        _session            = session;
    }
    
    [self setupRequestManager];
    
    return self;
}

#pragma mark - Accessor methods

- (void)setSession:(PTGSession *)session {
    _session = session;
    
    [self updateAuthorizationToken];
}

#pragma mark - Public methods

- (BOOL)isServerReachable
{
    return self.sessionManager.reachabilityManager.networkReachabilityStatus != AFNetworkReachabilityStatusNotReachable;
}


#pragma mark Private methods

- (NSURLSessionDataTask *)requestOperationWithMethod:(NSString*)method
                                          parameters:(NSDictionary*)parameters
                                       requestMethod:(NSString *)requestMethod
                                             success:(void(^)(id response))success
                                             failure:(void(^)(NSUInteger code, NSString *message))failure
{
    // ***************************************************************
    // *** Load dummy response for demo purposes
    // ***************************************************************
    if ([method isEqualToString:@"flights"]) {
    NSDictionary *response = [PTGDummyResponseFactory dummyResponseForFlights];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
        dispatch_async(self.sessionManager.completionQueue ?: dispatch_get_main_queue(), ^
                       {
                           success(response);
                       });
#pragma clang diagnostic pop
        return nil;
    }
    // ***************************************************************
    // *** Implementation for using real data continues below
    // ***************************************************************
    
    
    // Check if server is reachable
    if (self.isServerReachable == NO)
    {
        failure(PTGServiceErrorOffline, [PTGServiceParameters textForError:PTGServiceErrorOffline]);
        return nil;
    }
    
    NSString *fullPath = [NSURL URLWithString:method relativeToURL:self.sessionManager.baseURL].absoluteString;
    
    NSError *serializationError = nil;
    NSMutableURLRequest *request = [self.sessionManager.requestSerializer requestWithMethod:requestMethod
                                                                                  URLString:fullPath
                                                                                 parameters:parameters
                                                                                      error:&serializationError];
    
    if (serializationError)
    {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
        dispatch_async(self.sessionManager.completionQueue ?: dispatch_get_main_queue(), ^
                       {
                           failure(PTGServiceErrorSerialization, [PTGServiceParameters textForError:PTGServiceErrorSerialization]);
                       });
#pragma clang diagnostic pop
        
        return nil;
    }
    
    __block NSURLSessionDataTask *dataTask = nil;
    dataTask = [self.sessionManager dataTaskWithRequest:request
                                      completionHandler:^(NSURLResponse * response, id responseObject, NSError *error)
                {
                    PTGServiceError errorCode = PTGServiceErrorUnknown;
                    
                    // Handle http errors
                    if (error) {
                        PTGServiceError code = PTGServiceErrorUnknown;
                        if (error.code == NSURLErrorTimedOut) {
                            code = PTGServiceErrorTimeout;
                        }
                        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
                        dispatch_async(self.sessionManager.completionQueue ?: dispatch_get_main_queue(), ^
                                       {
                                           failure(errorCode, [PTGServiceParameters textForError:errorCode]);
                                       });
#pragma clang diagnostic pop
                    }
                    else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wgnu"
                        dispatch_async(self.sessionManager.completionQueue ?: dispatch_get_main_queue(), ^
                                       {
                                           success(responseObject);
                                       });
#pragma clang diagnostic pop
                    }
                }];
    
    [dataTask resume];
    
    return dataTask;
}

- (void)setupRequestManager
{
    // Initialize Session Manager
    [self setSessionManager:[[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:self.baseURL]
                                                     sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]]];

    self.sessionManager.completionQueue = dispatch_queue_create("backgroundCompletionQueue", NULL);
    [self.sessionManager setResponseSerializer:[AFJSONResponseSerializer serializer]];

    [self.sessionManager setRequestSerializer:[AFJSONRequestSerializer serializer]];

    [self.sessionManager.requestSerializer setValue:kPTGServiceApplicationToken
                                 forHTTPHeaderField:@"X-Application-Token"];

    [self updateAuthorizationToken];

    [self.sessionManager setSecurityPolicy:[AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone]];

    NSOperationQueue *operationQueue        = self.sessionManager.operationQueue;

    NSURL *googleURL = [NSURL URLWithString:@"http://google.com"];
    self.sessionManager.reachabilityManager = [AFNetworkReachabilityManager managerForDomain:googleURL.host];

    __weak typeof(self) weakSelf = self;
    [self.sessionManager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
     {
         BOOL reachability = NO;
         switch (status)
         {
             case AFNetworkReachabilityStatusReachableViaWWAN:
             case AFNetworkReachabilityStatusReachableViaWiFi:
                 [operationQueue setSuspended:NO];
                 reachability               = YES;
                 break;
             case AFNetworkReachabilityStatusNotReachable:
             default:
                 [operationQueue setSuspended:YES];
                 reachability               = NO;
                 break;
         }

         if (kPTGService_NetworkStateStatus != status)
         {
             kPTGService_NetworkStateStatus = status;

             if ([weakSelf.delegate respondsToSelector:@selector(reachabilityStatusChanged:)]) {
                 [weakSelf.delegate reachabilityStatusChanged:kPTGService_NetworkStateStatus];
             }
         }
     }];

    [self.sessionManager.reachabilityManager startMonitoring];
}

- (void)updateAuthorizationToken {
    [self.sessionManager.requestSerializer setValue:self.session.token
                                 forHTTPHeaderField:@"Authorization"];
}

@end
