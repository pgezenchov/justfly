//
//  PTGServiceClient.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTGSession.h"
#import "PTGServiceParameters.h"
#import <AFNetworking.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PTGServiceClientDelegate <NSObject>

- (void)reachabilityStatusChanged:(AFNetworkReachabilityStatus)reachabilityStatus;
- (void)didReceiveUnauthorizedError;

@end

@interface PTGServiceClient : NSObject

@property (nonatomic, weak) id<PTGServiceClientDelegate> delegate;
@property (nonatomic, strong) PTGSession *session;

// Constructors

- (instancetype)initWithEnvironment:(PTGServiceEnvironment)environment
                            session:(PTGSession* _Nullable)session;

- (NSURLSessionDataTask *)requestOperationWithMethod:(NSString*)method
                                          parameters:(NSDictionary*)params
                                       requestMethod:(NSString *)requestMethod
                                             success:(void(^)(id response))success
                                             failure:(void(^)(NSUInteger code, NSString *message))failure;

- (BOOL)isServerReachable;

@end

NS_ASSUME_NONNULL_END
