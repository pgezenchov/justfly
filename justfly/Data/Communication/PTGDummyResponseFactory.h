//
//  PTGDummyResponseFactory.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PTGDummyResponseFactory : NSObject

+ (NSDictionary*)dummyResponseForFlights;

@end

NS_ASSUME_NONNULL_END
