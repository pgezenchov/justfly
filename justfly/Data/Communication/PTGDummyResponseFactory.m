//
//  PTGDummyResponseFactory.m
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGDummyResponseFactory.h"

@implementation PTGDummyResponseFactory

+ (NSDictionary*)dummyResponseForFlights {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"flights" ofType:@"json"];
    NSData *dioctionaryData = [NSData dataWithContentsOfFile:filePath];
    
    NSError *error;
    NSDictionary * fligtsResponse = [NSJSONSerialization JSONObjectWithData:dioctionaryData
                                                                    options:NSJSONReadingMutableContainers
                                                                      error:&error];
    
    if (error != nil)
    {
        NSLog(@"Error parsing JSON:\n%@",error.userInfo);
        return nil;
    }
    
    return fligtsResponse;
}

@end
