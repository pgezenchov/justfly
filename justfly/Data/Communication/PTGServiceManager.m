//
//  PTGServiceManager.m
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGServiceManager.h"
#import "PTGDummyResponseFactory.h"
#import "PTGAirline.h"
#import "PTGFlight.h"
#import "PTGSearchFlightsResponse.h"
#import "PTGSearchFlightsRequest.h"

@interface PTGServiceManager ()

@property (nonatomic, strong) NSMutableDictionary *airportCachedResults;

@end

@implementation PTGServiceManager

#pragma mark - Constructors

- (instancetype)init
{
    self = [super init];
    if (self) {
        _serviceClient          = [PTGServiceClient new];
        _airportCachedResults   = [NSMutableDictionary new];
    }
    return self;
}

#pragma mark - Public methods

- (void)getAirportsForQuery:(NSString*)query
                    success:(void(^)(NSArray<PTGAirport*> *airports))success
                     falure:(void(^)(NSString *message))failure {
    if (success) {
        if (query.length == 0) {
            success(nil);
        }
        
        NSArray *cachedResult = [self getCachedAirportsForQuery:query];
        if (cachedResult.count > 0) {
            success(cachedResult);
        }
    }
    
    [self.serviceClient requestOperationWithMethod:@"airports"
                                        parameters:@{
                                                     @"name": query
                                                     }
                                     requestMethod:@"GET"
                                           success:^(id  _Nonnull response) {
                                               if (success) {
                                                   if ([response isKindOfClass:[NSArray class]]) {
                                                       NSMutableArray *airports = [NSMutableArray new];
                                                       
                                                       for (NSDictionary *airportDictionary in response) {
                                                           PTGAirport *airport = [[PTGAirport alloc] initWithDictionary:airportDictionary error:nil];
                                                           
                                                           if (airport) {
                                                               [airports addObject:airport];
                                                           }
                                                       }
                                                       
                                                       success(airports);
                                                   }
                                               }
                                           }
                                           failure:^(NSUInteger code, NSString * _Nonnull message) {
                                               if (failure) {
                                                   failure(message);
                                               }
                                           }];
}

- (void)getFlightsWithRequest:(PTGSearchFlightsRequest*)request
                      success:(void(^)(PTGSearchFlightsResponse *response))success
                      failure:(void(^)(NSString *message))failure {
    [self.serviceClient requestOperationWithMethod:@"flights"
                                        parameters:request.toDictionary
                                     requestMethod:@"GET"
                                           success:^(id  _Nonnull response) {
                                               if (success) {
                                                   if ([response isKindOfClass:[NSDictionary class]]) {
                                                       NSError *error;
                                                       PTGSearchFlightsResponse *parsedResponse = [[PTGSearchFlightsResponse alloc] initWithDictionary:response error:&error];
                                                       
                                                       success(parsedResponse);
                                                   }
                                               }
                                           }
                                           failure:^(NSUInteger code, NSString * _Nonnull message) {
                                               if (failure) {
                                                   failure(message);
                                               }
                                           }];
}

#pragma mark - Private methods

- (NSArray<PTGAirport*> *)getCachedAirportsForQuery:(NSString*)query {
    NSArray *cachedResult = self.airportCachedResults[query];
    
    // Results can be cached for a limited time.
    // When accessing cache old records can be cleaned here.
    
    return cachedResult;
}

@end
