//
//  PTGServiceManager.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTGServiceClient.h"
#import "PTGAirport.h"
#import "PTGItineary.h"
#import "PTGBestPrice.h"
#import "PTGSearchFlightsRequest.h"
#import "PTGSearchFlightsResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGServiceManager : NSObject

@property (nonatomic, strong) PTGServiceClient *serviceClient;

- (void)getAirportsForQuery:(NSString *)query
                    success:(void(^)(NSArray<PTGAirport*> *airports))success
                     falure:(void(^)(NSString *message))failure;

- (void)getFlightsWithRequest:(PTGSearchFlightsRequest*)request
                      success:(void(^)(PTGSearchFlightsResponse *response))success
                      failure:(void(^)(NSString *message))failure;

@end

NS_ASSUME_NONNULL_END
