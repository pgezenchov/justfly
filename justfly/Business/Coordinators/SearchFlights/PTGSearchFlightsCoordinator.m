//
//  PTGSearchFlightsCoordinator.m
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PTGSearchFlightsCoordinator.h"
#import "PTGSearchFlightsViewController.h"
#import "PTGAirport.h"
#import "PTGApplicationManager.h"


@interface PTGSearchFlightsCoordinator() <PTGSearchFlightsViewControllerDelegate>

@property (nonatomic, strong) PTGSearchFlightsViewController *viewController;

@property (nonatomic, strong) PTGAirportsAutocompleteDataSource *airportAutocompleteDataSource;
@property (nonatomic, strong) NSArray<PTGAirport*> *airportResults;

@property (nonatomic, strong) PTGAirport *fromAirport;
@property (nonatomic, strong) PTGAirport *toAirport;

@end

@implementation PTGSearchFlightsCoordinator

- (void)start {
    self.viewController = [PTGSearchFlightsViewController new];
    self.airportAutocompleteDataSource = [PTGAirportsAutocompleteDataSource new];
    
    self.viewController.airportAutocompleteTableView.dataSource = self.airportAutocompleteDataSource;
    self.viewController.delegate = self;
    
    [self.navigationController pushViewController:self.viewController animated:NO];
}

#pragma mark - PTGSearchFlightsViewControllerDelegate delegate

- (void)updateAirportResultForQuery:(NSString *)query completion:(void (^)(BOOL success))completion {
        // If no cached results load from server and cache results
        // success
    if (!completion) {
        return;
    }
    
    [[PTGApplicationManager sharedManager].serviceManager getAirportsForQuery:query
                                                                      success:^(NSArray<PTGAirport *> * _Nonnull airports) {
                                                                          self.airportResults = airports;
                                                                          [self reloadAirportsList];
                                                                          completion(YES);
                                                                      }
                                                                       falure:^(NSString * _Nonnull message) {
                                                                           completion(NO);
                                                                       }];
}

- (void)didSelectFromAirportAtIndex:(NSUInteger)index {
    if (self.airportResults.count > index){
        self.fromAirport = self.airportResults[index];
        [self.viewController updateFromAirportWithText:self.fromAirport.fullName];
    }
}

- (void)didSelectToAirportAtIndex:(NSUInteger)index {
    if (self.airportResults.count > index){
        self.toAirport = self.airportResults[index];
        [self.viewController updateToAirportWithText:self.toAirport.fullName];
    }
}

- (void)searchFlightsForDepartureDate:(NSString*)departureDate
                           returnDate:(NSString*)returnDate
                      passengersCount:(NSUInteger)passengersCount
                        andCabinClass:(PTGCabinClass)cabinClass {
    // Check input and show error if invalid
    
    // Crate flights request object
    PTGSearchFlightsRequest *request = [[PTGSearchFlightsRequest alloc] initWithDepartureAirportCode:@"VAR" //self.fromAirport.code (passing static data for demo)
                                                                           arrivalAirportCode:@"FRA" // self.toAirport.code (passing static data for demo)
                                                                                    departureDate:departureDate
                                                                                       returnDate:returnDate
                                                                                       flightType:PTGFlightTypeRoundTrip
                                                                                      adultsCount:passengersCount
                                                                                       cabinClass:cabinClass
                                                                                        pageIndex:0
                                                                                         pageSize:10
                                                                                     currencyCode:nil
                                                                                     itinearyType:PTGItinearyTypeCheapest];
    
    [self.delegate searchFlightsCoordinator:self wantsToProceedWithFlightsRequest:request];
}

#pragma mark - Private methods

- (void)reloadAirportsList {
    // Update data source with results
    self.airportAutocompleteDataSource.airports = self.airportResults;
    
    // Reload table
    [self.viewController.airportAutocompleteTableView reloadData];
}

@end
