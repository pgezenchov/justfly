//
//  PTGSearchFlightsCoordinator.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTGBaseCoordinator.h"
#import "PTGSearchFlightsRequest.h"

NS_ASSUME_NONNULL_BEGIN

@class PTGSearchFlightsCoordinator;

@protocol PTGSearchFlightsCoordinatorDelegate <NSObject>

- (void)searchFlightsCoordinator:(PTGSearchFlightsCoordinator *)coordinator wantsToProceedWithFlightsRequest:(PTGSearchFlightsRequest *)request;

@end

@interface PTGSearchFlightsCoordinator : PTGBaseCoordinator

@property (nonatomic, weak) id<PTGSearchFlightsCoordinatorDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
