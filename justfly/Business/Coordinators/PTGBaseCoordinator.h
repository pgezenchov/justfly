//
//  PTGBaseCoordinator.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PTGBaseCoordinator : NSObject

@property (nonatomic, strong) UINavigationController *navigationController;

- (instancetype)initWithNavigationController:(UINavigationController * _Nullable)navigationController NS_DESIGNATED_INITIALIZER;

- (void)start;

@end

NS_ASSUME_NONNULL_END
