//
//  PTGBookingConfirmationCoordinator.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGBaseCoordinator.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGBookingConfirmationCoordinator : PTGBaseCoordinator

@end

NS_ASSUME_NONNULL_END
