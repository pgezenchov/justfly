//
//  PTGSearchFlightsResultCoordinator.m
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGSearchFlightsResultCoordinator.h"
#import "PTGApplicationManager.h"
#import "PTGSearchFlightsResultViewController.h"
#import "PTGSearchFlightsResultDataSource.h"

@interface PTGSearchFlightsResultCoordinator () <PTGSearchFlightsResultViewControllerDelegate>

@property (nonatomic, strong) PTGSearchFlightsRequest *searchRequest;
@property (nonatomic, strong) PTGSearchFlightsResultViewController *viewController;
@property (nonatomic, strong) PTGSearchFlightsResultDataSource *flightsDataSource;
@property (nonatomic, strong) NSArray<PTGItineary*> *cheapestItinaries;
@property (nonatomic, strong) NSArray<PTGItineary*> *flexibleItinaries;
@property (nonatomic, strong) NSArray<PTGItineary*> *directItinaries;
@property (nonatomic) PTGItinearyType lastTypeFetch;

@end

@implementation PTGSearchFlightsResultCoordinator

- (instancetype)initWithNavigationController:(UINavigationController *)navigationController andFlightsRequest:(PTGSearchFlightsRequest *)request {
    self = [super initWithNavigationController:navigationController];
    if (self) {
        _searchRequest = request;
    }
    return self;
}

- (void)start {
    [PTGApplicationManager sharedManager].applicationCoordinator.busy = YES;

    [[PTGApplicationManager sharedManager].serviceManager getFlightsWithRequest:self.searchRequest
                                                                        success:^(PTGSearchFlightsResponse *response) {
                                                                            // Delay response for demo
                                                                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^
                                                                                           {
                                                                                               // Hide loading
                                                                                               [PTGApplicationManager sharedManager].applicationCoordinator.busy = NO;

                                                                                               self.viewController = [PTGSearchFlightsResultViewController new];
                                                                                               self.flightsDataSource = [PTGSearchFlightsResultDataSource new];
                                                                                               self.flightsDataSource.itinearies = response.itinearies;
                                                                                               self.cheapestItinaries = response.itinearies;
                                                                                               
                                                                                               self.viewController.dataSource = self.flightsDataSource;
                                                                                               self.viewController.delegate = self;
                                                                                               

                                                                                               [self.navigationController pushViewController:self.viewController animated:YES];
                                                                                           });
                                                                        }
                                                                        failure:^(NSString * _Nonnull message) {
                                                                            [PTGApplicationManager sharedManager].applicationCoordinator.busy = NO;
                                                                            // Show error
                                                                        }];
}

#pragma mark - PTGSearchFlightsResultViewControllerDelegate methods

- (void)didSelectTabAtIndex:(NSUInteger)index {
    // Load itinaries from memory if already fetched
    switch (index) {
        case PTGItinearyTypeCheapest:
            if (self.cheapestItinaries.count != 0) {
                [self reloadItinariesListWithItinaries:self.cheapestItinaries forItinearyType:PTGItinearyTypeCheapest];
                return;
            }
            break;
        case PTGItinearyTypeDirect:
            if (self.directItinaries.count != 0) {
                [self reloadItinariesListWithItinaries:self.directItinaries forItinearyType:PTGItinearyTypeDirect];
                return;
            }
            break;
        case PTGItinearyTypeFlexible:
            if (self.flexibleItinaries.count != 0) {
                [self reloadItinariesListWithItinaries:self.flexibleItinaries forItinearyType:PTGItinearyTypeFlexible];
                return;
            }
            break;
    }
    
    // Fetch itinaries
    self.searchRequest.itinearyType = [PTGSearchFlightsRequest stringForItinearyType:(PTGItinearyType)index];
    self.lastTypeFetch = (PTGItinearyType)index;
    
    [PTGApplicationManager sharedManager].applicationCoordinator.busy = YES;
    [[PTGApplicationManager sharedManager].serviceManager getFlightsWithRequest:self.searchRequest
                                                                        success:^(PTGSearchFlightsResponse *response) {
                                                                            // Delay response for demo
                                                                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^
                                                                                           {
                                                                                               // Hide loading
                                                                                               [PTGApplicationManager sharedManager].applicationCoordinator.busy = NO;
                                                                                               
                                                                                               [self reloadItinariesListWithItinaries:response.itinearies forItinearyType:self.lastTypeFetch];
                                                                                           });
                                                                        }
                                                                        failure:^(NSString * _Nonnull message) {
                                                                            [PTGApplicationManager sharedManager].applicationCoordinator.busy = NO;
                                                                            // Show error
                                                                        }];
}

- (void)didSelectRowAtIndex:(NSUInteger)index {
    [PTGApplicationManager sharedManager].applicationCoordinator.busy = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^
                   {
                       // Hide loading
                       [PTGApplicationManager sharedManager].applicationCoordinator.busy = NO;
                       // Load itineary for index
                       [self.delegate proceedToDetailsForItineary:nil];
                   });
}

#pragma mark - Private methods

- (void)reloadItinariesListWithItinaries:(NSArray<PTGItineary*>*)itinaries forItinearyType:(PTGItinearyType)type {
    switch (type) {
        case PTGItinearyTypeCheapest:
            self.cheapestItinaries = itinaries;
            break;
        case PTGItinearyTypeDirect:
            self.directItinaries = itinaries;
            break;
        case PTGItinearyTypeFlexible:
            self.flexibleItinaries = itinaries;
            break;
    }
    
    // Update data source
    self.flightsDataSource.itinearies = itinaries;
    
    // Reload table view
    [self.viewController.fligtsResultTableView reloadData];
}


@end
