//
//  PTGSearchFlightsResultCoordinator.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PTGBaseCoordinator.h"
#import "PTGItineary.h"
#import "PTGSearchFlightsRequest.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PTGSearchFlightsResultCoordinatorDelegate <NSObject>

- (void)proceedToDetailsForItineary:(PTGItineary * _Nullable)itineary;
- (void)failedFlightsRequestWithMessage:(NSString *)message;

@end

@interface PTGSearchFlightsResultCoordinator : PTGBaseCoordinator

- (instancetype)initWithNavigationController:(UINavigationController * _Nullable)navigationController
                              andFlightsRequest:(PTGSearchFlightsRequest * _Nullable)request;

@property (nonatomic, weak) id<PTGSearchFlightsResultCoordinatorDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
