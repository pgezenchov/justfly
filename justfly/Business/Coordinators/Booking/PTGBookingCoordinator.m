//
//  PTGBookingCoordinator.m
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGBookingCoordinator.h"
#import "PTGBookingViewController.h"

@interface PTGBookingCoordinator ()<PTGBookingViewControllerDelegate>

@property (nonatomic, strong) PTGBookingViewController *viewController;

@end

@implementation PTGBookingCoordinator

- (instancetype)initWithNavigationController:(UINavigationController * _Nullable)navigationController
                                 andItineary:(PTGItineary * _Nullable)itineary {
    self = [super initWithNavigationController:navigationController];
    if (self) {
        
    }
    return self;
}

- (void)start {
    self.viewController = [PTGBookingViewController new];
    self.viewController.delegate = self;
    
    [self.navigationController pushViewController:self.viewController animated:YES];
}

#pragma mark - PTGBookingViewControllerDelegate methods

- (void)didConfirm {
    [self.delegate proceedToConfimrationWithBookingTransaction:nil];
}

@end
