//
//  PTGBookingCoordinator.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGBaseCoordinator.h"
#import "PTGBookingTransaction.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PTGBookingCoordinatorDelegate <NSObject>

- (void)proceedToConfimrationWithBookingTransaction:(PTGBookingTransaction * _Nullable)bookingTransaction;

@end

@interface PTGBookingCoordinator : PTGBaseCoordinator

- (instancetype)initWithNavigationController:(UINavigationController * _Nullable)navigationController
                                 andItineary:(PTGItineary * _Nullable)itineary;

@property (nonatomic, weak) id<PTGBookingCoordinatorDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
