//
//  PTGBaseCoordinator.m
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGBaseCoordinator.h"

@implementation PTGBaseCoordinator

- (instancetype)init {
    return  [self initWithNavigationController:nil];
}

- (instancetype)initWithNavigationController:(UINavigationController *)navigationController {
    self = [super init];
    if (self) {
        _navigationController = navigationController;
    }
    return self;
}

- (void)start {
    // Implementd in subclasses
}

@end
