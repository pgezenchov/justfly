//
//  PTGScreenManager.m
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGApplicationCoordinator.h"
#import "PTGSearchFlightsViewController.h"
#import "PTGTheme.h"
#import "PTGSearchFlightsCoordinator.h"
#import <SVProgressHUD.h>
#import "PTGApplicationManager.h"
#import "PTGSearchFlightsResultCoordinator.h"
#import "PTGBookingCoordinator.h"
#import "PTGBookingConfirmationCoordinator.h"
#import "PTGBookingSuccessCoordinator.h"

@interface PTGApplicationCoordinator ()<PTGSearchFlightsCoordinatorDelegate, PTGSearchFlightsResultCoordinatorDelegate, PTGBookingCoordinatorDelegate>

@property (nonatomic, strong) PTGSearchFlightsCoordinator           *searchFlightsCoordinator;
@property (nonatomic, strong) PTGSearchFlightsResultCoordinator     *searchFlightsResultCoordinator;
@property (nonatomic, strong) PTGBookingCoordinator                 *bookingCoordinator;
@property (nonatomic, strong) PTGBookingConfirmationCoordinator     *bookingConfirmationCoordinator;
@property (nonatomic, strong) PTGBookingSuccessCoordinator          *bookingSuccessCoordinator;

@end

@implementation PTGApplicationCoordinator

#pragma mark - Accessor methods

- (void)setBusy:(BOOL)busy {
    if (_busy != busy) {
        _busy = busy;
        
        // Show/Hide global loading
        if (_busy) {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [SVProgressHUD show];
                           });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [SVProgressHUD dismiss];
                           });
        }
    }
}

#pragma mark - Public methods

- (void)start {
    self.searchFlightsCoordinator = [[PTGSearchFlightsCoordinator alloc] initWithNavigationController:self.navigationController];
    self.searchFlightsCoordinator.delegate = self;
    [self.searchFlightsCoordinator start];
}

#pragma mark - PTGSearchFlightsCoordinatorDelegate methods

- (void)searchFlightsCoordinator:(nonnull PTGSearchFlightsCoordinator *)coordinator wantsToProceedWithFlightsRequest:(nonnull PTGSearchFlightsRequest *)request {
    self.searchFlightsResultCoordinator = [[PTGSearchFlightsResultCoordinator alloc] initWithNavigationController:self.navigationController andFlightsRequest:request];
    self.searchFlightsResultCoordinator.delegate = self;
    [self.searchFlightsResultCoordinator start];
    
}

#pragma mark - PTGSearchFlightsResultCoordinatorDelegate methods

- (void)proceedToDetailsForItineary:(PTGItineary *)itineary {
    self.bookingCoordinator = [[PTGBookingCoordinator alloc] initWithNavigationController:self.navigationController andItineary:nil];
    self.bookingCoordinator.delegate = self;
    [self.bookingCoordinator start];
}

- (void)failedFlightsRequestWithMessage:(nonnull NSString *)message {
    
}

#pragma mark - PTGBookingCoordinatorDelegate methods

- (void)proceedToConfimrationWithBookingTransaction:(PTGBookingTransaction * _Nullable)bookingTransaction { 
    
}

@end
