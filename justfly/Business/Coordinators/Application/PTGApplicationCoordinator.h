//
//  PTGApplicationCoordinator.h
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PTGSearchFlightsRequest.h"
#import "PTGSearchFlightsResponse.h"
#import "PTGBaseCoordinator.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGApplicationCoordinator : PTGBaseCoordinator

// When YES full screen loading is shown and UI is blocked.
@property (nonatomic) BOOL busy;

@end

NS_ASSUME_NONNULL_END
