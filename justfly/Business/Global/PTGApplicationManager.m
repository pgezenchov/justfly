//
//  PTGApplicationManager.m
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGApplicationManager.h"
#import "PTGTheme.h"

@interface PTGApplicationManager ()<PTGServiceClientDelegate>

@end

@implementation PTGApplicationManager

#pragma mark - Constructors

+ (instancetype)sharedManager
{
    static PTGApplicationManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^
                  {
                      _sharedManager = [PTGApplicationManager new];
                  });
    
    return _sharedManager;
}

- (void)setup {
    // Create root view controller or application coordinator
    UINavigationController *navigationController = [UINavigationController new];
    _applicationCoordinator = [[PTGApplicationCoordinator alloc] initWithNavigationController:navigationController];
    [_applicationCoordinator start];
    
    _serviceManager         = [PTGServiceManager new];
    _serviceManager.serviceClient.delegate = self;
    
    [PTGTheme styleAppearence];
}

#pragma mark - PTGServiceClientDelegate methods

- (void)reachabilityStatusChanged:(AFNetworkReachabilityStatus)reachabilityStatus {
    
}

- (void)didReceiveUnauthorizedError {
    
}

@end
