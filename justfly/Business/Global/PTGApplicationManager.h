//
//  PTGApplicationManager.h
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PTGApplicationCoordinator.h"
#import "PTGServiceManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGApplicationManager : NSObject

@property (nonatomic, strong, readonly) PTGApplicationCoordinator *applicationCoordinator;
@property (nonatomic, strong, readonly) PTGServiceManager *serviceManager;

+ (instancetype)sharedManager;

// Creates all singleton instances accessible globally
- (void)setup;

@end

NS_ASSUME_NONNULL_END
