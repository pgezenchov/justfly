//
//  UIViewController+TopController.h
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (TopController)

+ (UIViewController*)topMostController;

@end

NS_ASSUME_NONNULL_END
