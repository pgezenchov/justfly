//
//  UIViewController+TopController.m
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "UIViewController+TopController.h"

@implementation UIViewController (TopController)

+ (UIViewController *)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController != nil) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

@end
