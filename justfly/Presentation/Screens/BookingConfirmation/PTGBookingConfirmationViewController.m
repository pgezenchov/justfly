//
//  PTGBookingConfirmationViewController.m
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGBookingConfirmationViewController.h"

@interface PTGBookingConfirmationViewController ()

@end

@implementation PTGBookingConfirmationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)payNowTapped:(id)sender {
    [self.delegate didTapPayNowButton];
}

@end
