//
//  PTGBookingConfirmationViewController.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTGBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PTGBookingConfirmationViewControllerDelegate <NSObject>

- (void)didTapPayNowButton;

@end

@interface PTGBookingConfirmationViewController : PTGBaseViewController

@property (nonatomic, weak) id<PTGBookingConfirmationViewControllerDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
