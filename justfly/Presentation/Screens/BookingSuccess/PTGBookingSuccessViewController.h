//
//  PTGBookingSuccessViewController.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTGBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGBookingSuccessViewController : PTGBaseViewController

@end

NS_ASSUME_NONNULL_END
