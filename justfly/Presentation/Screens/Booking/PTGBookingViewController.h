//
//  PTGBookingViewController.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTGBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PTGBookingViewControllerDelegate <NSObject>

- (void)didConfirm;

@end

@interface PTGBookingViewController : PTGBaseViewController

@property (nonatomic, weak) id<PTGBookingViewControllerDelegate> delegate;


@end

NS_ASSUME_NONNULL_END
