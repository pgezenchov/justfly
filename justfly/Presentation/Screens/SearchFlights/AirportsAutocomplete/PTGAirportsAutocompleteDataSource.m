//
//  PTGAirportsAutocompleteDataSource.m
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGAirportsAutocompleteDataSource.h"
#import "PTGAirportTableViewCell.h"

@implementation PTGAirportsAutocompleteDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    // Style airport result cell and return
    return nil;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.airports.count;
}

@end
