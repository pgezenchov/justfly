//
//  PTGAirportTableViewCell.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PTGAirportTableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
