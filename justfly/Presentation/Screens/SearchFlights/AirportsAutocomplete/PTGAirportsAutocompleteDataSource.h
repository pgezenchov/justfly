//
//  PTGAirportsAutocompleteDataSource.h
//  justfly
//
//  Created by Petar Gezenchov on 13/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PTGAirport.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGAirportsAutocompleteDataSource : NSObject<UITableViewDataSource>

@property (nonatomic, weak) NSArray<PTGAirport*> *airports;

@end

NS_ASSUME_NONNULL_END
