//
//  PTGSearchFlightsViewController.h
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTGBaseViewController.h"
#import "PTGAirportsAutocompleteDataSource.h"
#import "PTGFlight.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PTGSearchFlightsViewControllerDelegate <NSObject>

- (void)updateAirportResultForQuery:(NSString*)query completion:(void (^)(BOOL success))completion;
- (void)didSelectFromAirportAtIndex:(NSUInteger)index;
- (void)didSelectToAirportAtIndex:(NSUInteger)index;
- (void)searchFlightsForDepartureDate:(NSString*)departureDate
                           returnDate:(NSString* _Nullable)returnDate
                      passengersCount:(NSUInteger)passengersCount
                        andCabinClass:(PTGCabinClass)cabinClass;

@end

@interface PTGSearchFlightsViewController : PTGBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *airportAutocompleteTableView;

@property (nonatomic, strong) NSObject<PTGSearchFlightsViewControllerDelegate> *delegate;

- (void)updateFromAirportWithText:(NSString*)text;
- (void)updateToAirportWithText:(NSString*)text;

@end

NS_ASSUME_NONNULL_END
