//
//  PTGSearchFlightsViewController.m
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGSearchFlightsViewController.h"

@interface PTGSearchFlightsViewController () <UITextFieldDelegate, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *fromAirportTextField;
@property (weak, nonatomic) IBOutlet UITextField *toAirportTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *returnDateTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *autoCompleteTableVerticalContstraint;

@end

@implementation PTGSearchFlightsViewController

#pragma mark - Lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - Public methods

- (void)updateFromAirportWithText:(NSString*)text {
    // update text field values
}

- (void)updateToAirportWithText:(NSString*)text {
    // update text field values
}

#pragma mark - UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    // Move autocomplete table view under edited text field
    // Show laoding in text field
    // Update autocomplete results
    [self.delegate updateAirportResultForQuery:textField.text completion:^(BOOL success) {
        // Hide loading in text field
    }];
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Hide autocomplete table view
    // Notify the coordinator of the selected result
    if ([self.fromAirportTextField isFirstResponder]) {
        [self.delegate didSelectFromAirportAtIndex:indexPath.row];
    }
    else if ([self.toAirportTextField isFirstResponder]) {
        [self.delegate didSelectToAirportAtIndex:indexPath.row];
    }
}


#pragma mark - IBOutlet actions

- (IBAction)oneWayTapped:(id)sender {
    // Hide return date view by animating returnDateTopConstraint
}

- (IBAction)roundTripTapped:(id)sender {
    // Show return date view by animating returnDateTopConstraint
}

- (IBAction)searchFlightsTapped:(id)sender {
    [self.delegate searchFlightsForDepartureDate:@"2019/02/10"
                                      returnDate:nil
                                 passengersCount:1
                                   andCabinClass:PTGCabinClassEconomy];
}

- (IBAction)resetSearchTapped:(id)sender {
    // Reset all UI input elements
    // Reset all field values
}

@end
