//
//  PTGBaseViewController.m
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGBaseViewController.h"

@interface PTGBaseViewController ()

@end

@implementation PTGBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNavigationBar];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)setupNavigationBar {
    UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"PTGNavigationBarImageView"
                                                       owner:self
                                                     options:nil] firstObject];
    
    self.navigationItem.titleView = view;
}

@end
