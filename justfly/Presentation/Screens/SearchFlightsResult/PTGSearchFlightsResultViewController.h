//
//  PTGSearchFlightsResultViewController.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTGBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@protocol PTGSearchFlightsResultViewControllerDelegate <NSObject>

- (void)didSelectTabAtIndex:(NSUInteger)index;
- (void)didSelectRowAtIndex:(NSUInteger)index;

@end

@interface PTGSearchFlightsResultViewController : PTGBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *fligtsResultTableView;

@property (nonatomic, weak) id<PTGSearchFlightsResultViewControllerDelegate> delegate;
@property (nonatomic, weak) id<UITableViewDataSource> dataSource;



@end

NS_ASSUME_NONNULL_END
