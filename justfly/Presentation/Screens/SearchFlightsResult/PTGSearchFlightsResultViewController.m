//
//  PTGSearchFlightsResultViewController.m
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGSearchFlightsResultViewController.h"
#import "PTGApplicationManager.h"
#import "PTGTheme.h"
#import <QuartzCore/QuartzCore.h>
#import "PTGFlightResultTableViewCell.h"

@interface PTGSearchFlightsResultViewController ()<UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *departureDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *departureAirportLabel;
@property (weak, nonatomic) IBOutlet UILabel *arrivalAirportLabel;
@property (weak, nonatomic) IBOutlet UILabel *arrivalDateLabel;
@property (weak, nonatomic) IBOutlet UIView *summaryToggleView;
@property (weak, nonatomic) IBOutlet UIView *cheapestView;
@property (weak, nonatomic) IBOutlet UIView *directView;
@property (weak, nonatomic) IBOutlet UIView *flexibleView;

@end

@implementation PTGSearchFlightsResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.headerView.backgroundColor = [UIColor colorForType:PTGThemeColorPrimary];
    self.summaryToggleView.backgroundColor = [UIColor colorForType:PTGThemeColorPrimaryDark];
    
    [self removeHighlightFromView:self.directView];
    [self removeHighlightFromView:self.flexibleView];
    
    self.fligtsResultTableView.dataSource = self.dataSource;
    self.fligtsResultTableView.delegate = self;
    [self registerCellInTableView];
}

- (void)highlightView:(UIView*)view {
    [UIView animateWithDuration:.3f animations:^{
        view.backgroundColor = [UIColor whiteColor];
        view.layer.borderColor = [UIColor clearColor].CGColor;
        view.layer.borderWidth = .5f;
    }];
}

- (void)removeHighlightFromView:(UIView*)view {
    [UIView animateWithDuration:.3f animations:^{
        view.backgroundColor = [UIColor colorWithWhite:0.97f alpha:1];
        view.layer.borderColor = [UIColor colorWithWhite:0.93f alpha:1].CGColor;
        view.layer.borderWidth = .5f;
    }];
}

- (void)registerCellInTableView {
    [self.fligtsResultTableView registerClass:[PTGFlightResultTableViewCell class] forCellReuseIdentifier:@"PTGFlightResultTableViewCell"];
    [self.fligtsResultTableView registerNib:[UINib nibWithNibName:@"PTGFlightResultTableViewCell" bundle: nil]
                     forCellReuseIdentifier:@"PTGFlightResultTableViewCell"];
}

- (IBAction)cheapestButtonTapped:(id)sender {
    [self.delegate didSelectTabAtIndex:0];
    
    [self removeHighlightFromView:self.directView];
    [self removeHighlightFromView:self.flexibleView];
    [self highlightView:self.cheapestView];
}

- (IBAction)directButtonTapped:(id)sender {
    [self.delegate didSelectTabAtIndex:1];
    
    [self removeHighlightFromView:self.cheapestView];
    [self removeHighlightFromView:self.flexibleView];
    [self highlightView:self.directView];
}

- (IBAction)flexibleButtonTapped:(id)sender {
    [self.delegate didSelectTabAtIndex:2];
    
    [self removeHighlightFromView:self.directView];
    [self removeHighlightFromView:self.cheapestView];
    [self highlightView:self.flexibleView];
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.delegate didSelectRowAtIndex:indexPath.row];
}

@end
