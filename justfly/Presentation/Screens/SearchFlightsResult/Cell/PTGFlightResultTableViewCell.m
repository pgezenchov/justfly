//
//  PTGFlightResultTableViewCell.m
//  justfly
//
//  Created by Petar Gezenchov on 15/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGFlightResultTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@interface PTGFlightResultTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *returnConnectionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *connectionsLabel;
@property (weak, nonatomic) IBOutlet UIButton *priceButton;

@end

@implementation PTGFlightResultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.connectionsLabel.layer.cornerRadius = self.connectionsLabel.frame.size.height / 2;
    self.connectionsLabel.layer.masksToBounds = true;
    
    self.returnConnectionsLabel.layer.cornerRadius = self.connectionsLabel.frame.size.height / 2;
    self.returnConnectionsLabel.layer.masksToBounds = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)customizeWithPrice:(NSString*)price {
    [self.priceButton setTitle:price forState:UIControlStateNormal];
}

@end
