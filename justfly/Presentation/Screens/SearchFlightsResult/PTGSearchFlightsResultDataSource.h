//
//  PTGSearchFlightsResultDataSource.h
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PTGItineary.h"

NS_ASSUME_NONNULL_BEGIN

@interface PTGSearchFlightsResultDataSource : NSObject<UITableViewDataSource>

@property (nonatomic, weak) NSArray<PTGItineary *> *itinearies;

@end

NS_ASSUME_NONNULL_END
