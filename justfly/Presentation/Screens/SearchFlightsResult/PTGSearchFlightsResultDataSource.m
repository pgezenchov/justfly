//
//  PTGSearchFlightsResultDataSource.m
//  justfly
//
//  Created by Petar Gezenchov on 14/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGSearchFlightsResultDataSource.h"
#import "PTGFlightResultTableViewCell.h"

@implementation PTGSearchFlightsResultDataSource

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    PTGFlightResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PTGFlightResultTableViewCell" forIndexPath:indexPath];
    
    [cell customizeWithPrice:self.itinearies[indexPath.row].price];
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itinearies.count;
}

@end
