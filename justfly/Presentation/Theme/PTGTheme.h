//
//  PTGTheme.h
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIColor+PTGTheme.h"

NS_ASSUME_NONNULL_BEGIN


@interface PTGTheme : NSObject

+ (void)styleAppearence;

@end

NS_ASSUME_NONNULL_END
