//
//  UIColor+PTGTheme.h
//  justfly
//
//  Created by Petar Gezenchov on 15/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

// justly application colors
typedef NS_ENUM(NSUInteger, PTGThemeColors)
{
    // Blue
    PTGThemeColorPrimary,
    PTGThemeColorPrimaryDark
};


@interface UIColor (PTGTheme)

+ (UIColor *)colorForType:(PTGThemeColors)type;

@end

NS_ASSUME_NONNULL_END
