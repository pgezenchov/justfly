//
//  PTGTheme.m
//  justfly
//
//  Created by Petar Gezenchov on 12/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "PTGTheme.h"
#import "UIViewController+TopController.h"
#import "SVProgressHUD.h"


@implementation PTGTheme

+ (void)styleAppearence {
    // Navigation bar color
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorForType:PTGThemeColorPrimary]];
    
    // Navigation bar shadow
    UIViewController *topController = [UIViewController topMostController];
    UINavigationController *navigationController = [topController isKindOfClass:[UINavigationController class]] ? (UINavigationController*)topController : topController.navigationController;
    navigationController.navigationBar.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    navigationController.navigationBar.layer.shadowOffset = CGSizeMake(0.f, 2.f);
    navigationController.navigationBar.layer.shadowRadius = 4.0;
    navigationController.navigationBar.layer.shadowOpacity = 1.0;
    navigationController.navigationBar.layer.masksToBounds = NO;
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Set HUD loading color
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    [SVProgressHUD setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.65f]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    
    // Disable user interaction while HUD is on screen
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
}



@end
