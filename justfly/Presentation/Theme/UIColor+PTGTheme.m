//
//  UIColor+PTGTheme.m
//  justfly
//
//  Created by Petar Gezenchov on 15/04/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

#import "UIColor+PTGTheme.h"

#define UIColorFromRGB(rgbValue)    [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:((float)((rgbValue & 0xFF000000) >> 24)/255.0f)]

static NSUInteger PTGThemeColorsLUT[] = {
    
    // Blue
    0xFF485778, // PTGThemeColorPrimary:                72,87,120
    0xFF404F6D, // PTGThemeColorPrimaryDark:            64,79,109
};

@implementation UIColor (PTGTheme)

+ (UIColor *)colorForType:(PTGThemeColors)type {
    return UIColorFromRGB(PTGThemeColorsLUT[type]);
}

@end
