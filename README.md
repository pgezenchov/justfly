## Concept

Application is split into three layers with dedicated funtionality:

* **Presentation** - displays UI and handles user interactions. Receives data from business layer and presents it.
* **Business** - manages all user flows and processes in the application. Connects data with presentation layer.
* **Data** - responsible for fetching, caching, storing and loading data.

Business layer controls UI navigation with coordinators. This way view controllers are more focused on presenting data.

![alt text](https://files.dxr.cloud/GcdDv81jX5WsUbDUDBDi3dX98eSvwCLzzHuc8cjPbncl4UprPa4uUSbTCKNT)

---

## Roles

### Application Manager

Entry point for application. Initializes all global objects and setups application. Responsible for:

* initializing and holding reference to global objects
* initializing and setting up 3rd party libraries
* handling global application events as network status changes etc.

### Coordinators

Coordinators are building blocks for user flows. They take the job of navigating between screens from view controllers making them lighter and more reusable. Coordinators also implement business logic and are meant to perform a certain task.

A coordinator creates a view controller and presents it. View controller communicates events with the coordinator trough a delegate. The coordinator manages the controller's data source. Each coordinator has child coordinators and a parent. When finished with it's job the coordinator reports back to a parent coordinator trough a delegate. **Application Coordinator** is the main coordinator. It is initially created by Application Manager and is given control over the main UIWindow. **Application Coordinator** manages the application workflow.

Coordinators are responsible for:

* navigation between view controllers
* fetching data and updating view controllers
* implementing business logic and performing a certain task

### Service Manager

Fetches data from a server trough a **Service Client**. Responsible for:

* build request to fetch certain data
* mapping fetched data to objects
* caching responses

### Data Manager

Manages local storage. Responsible for:

* storing data into local storage
* retrieving data from local storage
* caching data

---

## Implemenation

Here is an example implementation of the user flow for starting and confirming a booking transaction:

1. **Application Coordinator** receives the selected itinieary from it's child coordinator for picking flights.
2. Application Coordinator starts **Booking Coordinator** by passing it a navigation controller to present booking controller in and the selected itineary.
3. Booking Coordinator creates a booking transaction and presents a booking controller. Trough a delegate gets notified when the controller receives taps for adding/removing options and performs server request.
3. On confirm Booking Coordinator notifies it's parent coordinator.
4. Application Coordinator checks for login and starts login flow with **Authentication Coordinator** if neccessary.
5. AuthenticationCoordinator presents authentication options and after selection creates a child coordinator to handle login/register.
6. Trough delegation result is reaching Application Coordinator which proceeds with the user flow.

![alt text](https://files.dxr.cloud/3bcVnsE2dkIRlZL5bOIrvDqHzcrknhymxuuCBf7n05nr4P5KREq3zzOgamDe)

## API

OpenAPI specification: [here](https://files.dxr.cloud/G2knqgMMs1S1WA8H7zMjG4JmNMSoHEvwqQeYaq71whJ8LG5WTKl3jIwpUTXo)
